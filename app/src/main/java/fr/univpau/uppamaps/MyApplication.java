package fr.univpau.uppamaps;

import android.app.Application;

public class MyApplication extends Application {
    private String mode;

    public String getMode(){
        return mode;
    }

    public void setMode(String mode){
        this.mode = mode;
    }
}

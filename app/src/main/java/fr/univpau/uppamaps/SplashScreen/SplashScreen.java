package fr.univpau.uppamaps.SplashScreen;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import fr.univpau.uppamaps.R;
import fr.univpau.uppamaps.Screens.ListCity;


public class SplashScreen extends AppCompatActivity {
    private boolean permission = false;
    private boolean gps = false;
    private boolean dontAskAgain = false;
    private boolean alreadyShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.greenUPPA));

    }

    @Override
    protected void onResume() {
        super.onResume();

        Vibrator vibrator = ((Vibrator) getSystemService(VIBRATOR_SERVICE));
        long[] vibratepattern = {0, 50, 600, 600};
        VibrationEffect vibe = VibrationEffect.createWaveform(vibratepattern, -1);
        vibrator.vibrate(vibe);

        new Handler().postDelayed(() -> {
            if(!dontAskAgain){
                checkPermissions();
                checkGpsIsActivated();
            }
            if(permission && gps){
                startActivityUppa();
            }
        }, 200);
    }

    private void checkGpsIsActivated(){
        LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled){
            if(!alreadyShow){
                buildAlertMessageNoGps();
            }
        }else{
            gps = true;
        }
    }

    private void buildAlertMessageNoGps() {
        alreadyShow = true;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("La localisation GPS n'est pas activé. Veuillez l'activer s'il vous plait.")
                .setCancelable(false)
                .setPositiveButton("Compris", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        alreadyShow = false;
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void checkPermissions(){
        if (ContextCompat.checkSelfPermission( this,
                Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }else{
            permission = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case 1:
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])){
                    Toast.makeText(this, "Permission rejetée", Toast.LENGTH_SHORT).show();
                }else if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if(ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                        Toast.makeText(this, "Permission approuvée", Toast.LENGTH_SHORT).show();
                        permission = true;
                    }
                }else{
                    Toast.makeText(this, "Permission rejetée définitive", Toast.LENGTH_SHORT).show();
                    dontAskAgain = true;
                    buildAlertMessageRejected();
                }
                return;
        }
    }

    public void buildAlertMessageRejected(){
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setMessage("L'application va fermer.\n" +
                "Veuillez autoriser l'accès dans les paramètres de l'application.")
                .setCancelable(false)
                .setPositiveButton("Compris", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finishAffinity();
                        System.exit(0);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void startActivityUppa(){
        Intent intent = new Intent(this, ListCity.class);
        new Handler().postDelayed(() -> {
            startActivity(intent);
            finish();
        }, 2000);
    }
}
package fr.univpau.uppamaps.Screens.Building;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import fr.univpau.uppamaps.R;

public class BuildingAdapter extends ArrayAdapter<BuildingItem> {
    private Context context;
    private ArrayList<BuildingItem> building;
    
    public BuildingAdapter(@NonNull Context context, ArrayList<BuildingItem> building) {
        super(context, 0, building);
        this.context = context;
        this.building = building;
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent){
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.activity_building_item, parent, false);
        }
        BuildingItem currentBuilding = building.get(position);
        TextView tv = convertView.findViewById(R.id.buildingName);
        tv.setText(currentBuilding.getName());
        if(currentBuilding.coordIsInit()){
            tv.setHint(currentBuilding.getCoord());
        }
        return convertView;
    }
}

package fr.univpau.uppamaps.Screens.City;

import android.content.Context;
import android.content.res.AssetManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class CityListener {
    private CityAdapter cityAdapter;
    private Context context;
    ArrayList<CityItem> cityList = new ArrayList<>();

    public CityListener(Context context, CityAdapter adapter){
        this.context = context;
        this.cityAdapter = adapter;
    }

    public CityAdapter getCityAdapter(){
        return cityAdapter;
    }

    public void clearAll(){
        cityList.clear();
        cityAdapter.clear();
    }

    public void csvParser(){
        AssetManager am = context.getAssets();
        try{
            InputStream is = am.open("exportbatimentsUPPA.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String csvLine;
            while((csvLine = reader.readLine())!= null){
                String[] data = csvLine.split(";");
                if(!isPresent(data[5]) && !data[5].equals("VILLE")){
                    CityItem cityItem = new CityItem(data[5],0);
                    cityList.add(cityItem);
                }
            }
        }catch (IOException e){
            System.out.println(e.toString());
        }
    }

    private Boolean isPresent(String name){
        Boolean present = false;
        int i = 0;
        while(!present && i < cityList.size()){
            if(cityList.get(i).getName().equals(name)){
                present = true;
            }
            i++;
        }
        return present;
    }

    public void getCitys(){
        for(CityItem city : cityList){
            cityAdapter.add(city);
        }
    }
}

package fr.univpau.uppamaps.Screens;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fr.univpau.uppamaps.MyApplication;
import fr.univpau.uppamaps.R;
import fr.univpau.uppamaps.Screens.Building.BuildingAdapter;
import fr.univpau.uppamaps.Screens.Building.BuildingItem;
import fr.univpau.uppamaps.Screens.Building.BuildingListener;

public class ListBuilding extends AppCompatActivity {
    private String cityName;
    private String localMode;
    private BuildingListener buildingListener;
    private ArrayList<BuildingItem> buildingItem = new ArrayList<>();
    private ListView list;
    private Context myapp;
    private SharedPreferences pref;
    private SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_building);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.greenUPPA));
        initVar();
    }

    private void initVar(){
        Intent oldIntent = getIntent();
        cityName = oldIntent.getStringExtra("cityName");
        TextView city = findViewById(R.id.cityName);
        city.setText(cityName);

        myapp = this.getApplication();
        pref = getPreferences(Activity.MODE_PRIVATE);
        ed = pref.edit();
        localMode = pref.getString("mode", "unknown");

        buildingListener = new BuildingListener(this, new BuildingAdapter(this, buildingItem), cityName);
        list = findViewById(R.id.buildingList);
        list.setAdapter(buildingListener.getBuildingAdapter());
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean permissions = false;
                boolean gps = false;
                if(checkPermissions()){
                    permissions = true;
                }
                if(checkGpsIsActivated()){
                    gps = true;
                }
                if(permissions && gps){
                    itemClicked(view);
                }
            }
        });
        buildingListener.csvParser();

        ImageButton btnpref = findViewById(R.id.btnpref);
        btnpref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListBuilding.this, Preference.class);
                startActivity(intent);
            }
        });

        LinearLayout linearBack = findViewById(R.id.linearBackApp);
        linearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageButton btnaide = findViewById(R.id.btnaide);
        btnaide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildMessageHelp();
            }
        });
    }

    private void itemClicked(View view){
        TextView tv = ((TextView) view.findViewById(R.id.buildingName));
        String name = tv.getText().toString();
        if(((MyApplication) myapp).getMode() == null){
            if(localMode.equals("unknown")){
                ed.putString("mode", "marche");
                ed.commit();
                if(!tv.getHint().toString().equals("1")){
                    redirectionMapCoord("w", tv.getHint().toString());
                }else{
                    redirectionMapName("w", name);
                }
            }else if(localMode.equals("marche")){
                if(!tv.getHint().toString().equals("1")){
                    redirectionMapCoord("w", tv.getHint().toString());
                }else{
                    redirectionMapName("w", name);
                }
            }else{
                if(!tv.getHint().toString().equals("1")){
                    redirectionMapCoord("b", tv.getHint().toString());
                }else{
                    redirectionMapName("b", name);
                }
            }
        }else if(((MyApplication) myapp).getMode().equals("marche")){
            ed.putString("mode", "marche");
            ed.commit();
            if(!tv.getHint().toString().equals("1")){
                redirectionMapCoord("w", tv.getHint().toString());
            }else{
                redirectionMapName("w", name);
            }
        }else{
            ed.putString("mode", "velo");
            ed.commit();
            if(!tv.getHint().toString().equals("1")){
                redirectionMapCoord("b", tv.getHint().toString());
            }else{
                redirectionMapName("b", name);
            }
        }
    }

    private void redirectionMapName(String mode, String bat){
        bat = bat.replaceAll(" ", "+");
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + bat + "," + cityName + "&mode=" + mode);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if(mapIntent.resolveActivity(getPackageManager()) != null){
            startActivity(mapIntent);
        }else{
            buildAlertMessageNoGmaps();
        }

    }

    private void redirectionMapCoord(String mode, String coord){
        Uri gmmIntentUri = Uri.parse("google.navigation:q=" + coord + "&mode=" + mode);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if(mapIntent.resolveActivity(getPackageManager()) != null){
            startActivity(mapIntent);
        }else{
            buildAlertMessageNoGmaps();
        }
    }

    private DialogInterface.OnClickListener getGoogleMapsListener()
    {
        return new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.maps"));
                startActivity(intent);
            }
        };
    }

    private boolean checkPermissions(){
        if (ContextCompat.checkSelfPermission( getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode){
            case 1:
                if(ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])){
                    Toast.makeText(this, "Permission rejetée", Toast.LENGTH_SHORT).show();
                }else if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if(ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                        Toast.makeText(this, "Permission approuvée", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this, "Permission rejetée définitive", Toast.LENGTH_SHORT).show();
                    buildAlertMessageRejected();
                }
                return;
        }
    }

    private void buildAlertMessageRejected(){
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setMessage("L'application va fermer, vous avez décidé d'interdire la permission d'accès à votre localisation.\n" +
                "S'il vous plait veuillez autoriser l'accès dans les paramètres de l'application.")
                .setCancelable(false)
                .setPositiveButton("Compris", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finishAffinity();
                        System.exit(0);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private boolean checkGpsIsActivated(){
        LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled){
            buildAlertMessageNoGps();
            return false;
        }
        return true;
    }

    private void buildAlertMessageNoGps() {
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setMessage("La localisation GPS n'est pas activé. Veuillez l'activer s'il vous plait.")
                .setCancelable(false)
                .setPositiveButton("Compris", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void buildAlertMessageNoGmaps() {
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setMessage("L'application Google Maps n'est pas installé. Veuillez l'installer s'il vous plait.")
                .setCancelable(false)
                .setPositiveButton("Compris", getGoogleMapsListener());
        final androidx.appcompat.app.AlertDialog alert = builder.create();
        alert.show();
    }

    public void buildMessageHelp(){
        String message = "Cette application vous aide à vous déplacer entre les différents bâtiments de l'UPPA.\n" +
                "- Sélectionnez une ville\n" +
                "- Sélectionnez un bâtiment\n" +
                "- Déplacez vous ! :)";
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setMessage(message)
                .setTitle("UPPA Maps")
                .setCancelable(false)
                .setPositiveButton("Compris", null);
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#B5BD00"));
    }
}
package fr.univpau.uppamaps.Screens;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

import fr.univpau.uppamaps.R;
import fr.univpau.uppamaps.Screens.City.CityAdapter;
import fr.univpau.uppamaps.Screens.City.CityItem;
import fr.univpau.uppamaps.Screens.City.CityListener;

public class ListCity extends AppCompatActivity {
    private ArrayList<CityItem> cityItem = new ArrayList<CityItem>();
    private CityListener cityListener;
    private ListView list;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_city);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.greenUPPA));
        initVar();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.loading, null));
        builder.setCancelable(false);
        dialog = builder.create();
        dialog.show();
        getList();
    }

    private void initVar(){
        cityListener = new CityListener(this, new CityAdapter(this, cityItem));
        list = findViewById(R.id.cityList);
        list.setAdapter(cityListener.getCityAdapter());
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
                Intent intent = new Intent(ListCity.this, ListBuilding.class);
                intent.putExtra("cityName", ((TextView)view.findViewById(R.id.cityName)).getText());
                startActivity(intent);
            }
        });
        ImageButton btnPref = findViewById(R.id.btnpref);
        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListCity.this, Preference.class);
                startActivity(intent);
            }
        });

        LinearLayout linearBack = findViewById(R.id.linearBackApp);
        linearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageButton btnaide = findViewById(R.id.btnaide);
        btnaide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buildMessageHelp();
            }
        });
    }

    public void buildMessageHelp(){
        String message = "Cette application vous aide à vous déplacer entre les différents bâtiments de l'UPPA.\n" +
                        "- Sélectionnez une ville\n" +
                        "- Sélectionnez un bâtiment\n" +
                        "- Déplacez vous ! :)";
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
        builder.setMessage(message)
                .setTitle("UPPA Maps")
                .setCancelable(false)
                .setPositiveButton("Compris", null);
        final AlertDialog alert = builder.create();
        alert.show();
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#B5BD00"));
    }


    private void getList(){
        cityListener.clearAll();
        cityListener.csvParser();
        cityListener.getCitys();
        dialog.dismiss();
    }
}
package fr.univpau.uppamaps.Screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

import fr.univpau.uppamaps.MyApplication;
import fr.univpau.uppamaps.R;

public class Preference extends AppCompatActivity {
    private Context myapp;
    private SharedPreferences pref;
    private SharedPreferences.Editor ed;
    private Switch modeDep;
    private String localMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.greenUPPA));
        initVar();
        setValueMode();
    }

    private void initVar(){
        myapp = this.getApplication();
        pref = getPreferences(Activity.MODE_PRIVATE);
        ed = pref.edit();
        localMode = pref.getString("mode", "unknown");
        modeDep = findViewById(R.id.modeDep);
        modeDep.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                modeDep.setChecked(isChecked);
                if(isChecked){
                    ((MyApplication) myapp).setMode("velo");
                    ed.putString("mode", "velo");
                    ed.commit();
                }else{
                    ((MyApplication) myapp).setMode("marche");
                    ed.putString("mode", "marche");
                    ed.commit();
                }
            }
        });

        LinearLayout linearBack = findViewById(R.id.linearBackApp);
        linearBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void setValueMode(){
        if(localMode.equals("unknown")){
            ed.putString("mode", "marche");
            ed.commit();
            ((MyApplication) myapp).setMode("marche");
        }else if(localMode.equals("marche")){
            modeDep.setChecked(false);
            ((MyApplication) myapp).setMode("marche");
        }else{
            modeDep.setChecked(true);
            ((MyApplication) myapp).setMode("velo");
        }
    }
}
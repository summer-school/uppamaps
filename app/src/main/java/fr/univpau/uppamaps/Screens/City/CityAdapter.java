package fr.univpau.uppamaps.Screens.City;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

import fr.univpau.uppamaps.R;

public class CityAdapter extends ArrayAdapter<CityItem> {
    private Context context;
    private ArrayList<CityItem> city;

    public CityAdapter(@NonNull Context context, ArrayList<CityItem> city) {
        super(context, 0, city);
        this.context = context;
        this.city = city;
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent){
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.activity_city_item, parent, false);
        }
        CityItem currentCity = city.get(position);
        TextView tv = convertView.findViewById(R.id.cityName);
        tv.setText(currentCity.getName());
        return convertView;
    }
}

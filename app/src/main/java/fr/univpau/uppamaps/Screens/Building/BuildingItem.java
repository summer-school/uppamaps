package fr.univpau.uppamaps.Screens.Building;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import fr.univpau.uppamaps.R;

public class BuildingItem extends AppCompatActivity {
    private String name;
    private String lat;
    private String lgt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building_item);
    }

    public BuildingItem(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public String getCoord(){
        return lat + "," + lgt;
    }

    public void setCoord(String lat, String lgt){
        this.lat = lat;
        this.lgt = lgt;
    }

    public boolean coordIsInit(){
        if(lat != null && lgt != null){
            return true;
        }
        return false;
    }
}
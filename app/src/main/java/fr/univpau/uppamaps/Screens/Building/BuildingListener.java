package fr.univpau.uppamaps.Screens.Building;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class BuildingListener {
    private BuildingAdapter buildingAdapter;
    private Context context;
    private String cityName;

    public BuildingListener(Context context, BuildingAdapter buildingAdapter, String cityName){
        this.context = context;
        this.buildingAdapter = buildingAdapter;
        this.cityName = cityName;
    }

    public BuildingAdapter getBuildingAdapter(){
        return buildingAdapter;
    }

    public void csvParser(){
        AssetManager am = context.getAssets();
        try{
            InputStream is = am.open("exportbatimentsUPPA.csv");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("Cp1252")));
            String csvLine;
            while((csvLine = reader.readLine())!= null){
                String[] data = csvLine.split(";");
                if(data[5].equals(this.cityName)){
                    BuildingItem buildingItem = new BuildingItem(data[2]);
                    if(data.length >= 9){
                        buildingItem.setCoord(data[8], data[9]);
                    }
                    buildingAdapter.add(buildingItem);
                }
            }
        }catch (IOException e){
            System.out.println(e.toString());
        }
    }
}

package fr.univpau.uppamaps.Screens.City;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;

import fr.univpau.uppamaps.R;

public class CityItem extends AppCompatActivity {
    private String cityName;
    private int distance;
    private CardView card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_item);

    }

    public CityItem(String name, int dist){
        this.cityName = name;
        this.distance = dist;
    }

    public String getName(){
        return this.cityName;
    }

    public int getDistance(){
        return this.distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
}